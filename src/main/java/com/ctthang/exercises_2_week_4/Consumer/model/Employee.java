package com.ctthang.exercises_2_week_4.Consumer.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.ToString;

@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id", scope = com.ctthang.exercises_2_week_4.Producer.model.Employee.class)
@Data
@ToString
public class Employee {
    
    private String empName;
    private String empId;
    private int salary;
    
}
