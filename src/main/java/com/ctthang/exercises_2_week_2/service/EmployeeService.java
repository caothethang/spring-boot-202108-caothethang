package com.ctthang.exercises_2_week_2.service;

import com.ctthang.exercises_2_week_2.entity.Employee;
import com.ctthang.exercises_2_week_2.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {
    
    @Autowired
    private EmployeeRepository employeeRepository;
    
    public List<Employee> getAll(){
        return employeeRepository.findAll();
    }
    
    public Employee getById(Long id){
        return employeeRepository.getById(id);
    }
    
    public Employee insert(Employee employee) {
        return employeeRepository.save(employee);
    }
    
    public Employee update(Long id, Employee newEmployee) {
        Optional<Employee> employeeData = employeeRepository.findById(id);
        if(employeeData.isPresent()){
            Employee employee = employeeData.get();
            employee.setName(newEmployee.getName());
            return employeeRepository.save(employee);
        }else {
            newEmployee.setId(id);
            return employeeRepository.save(newEmployee);
        }
    }
    
    public void deleteEmployee(Long id){
        employeeRepository.deleteById(id);
    }
}
