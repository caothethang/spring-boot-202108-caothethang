package com.ctthang.exercises_2_week_2.repository;

import com.ctthang.exercises_2_week_2.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee,Long> {

}
