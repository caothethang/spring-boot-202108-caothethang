package com.ctthang.exercises_2_week_2;

import com.ctthang.exercises_2_week_2.entity.Employee;
import com.ctthang.exercises_2_week_2.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
public class DemoCrudMySql implements CommandLineRunner {
    
    @Autowired
    private EmployeeService employeeService;
    
    public static void main(String[] args) {
        SpringApplication.run(DemoCrudMySql.class);
    }
    
    @Override
    public void run(String... args) throws Exception {
//        for(long i = 1;i<20;i++){
//            String name = "Employee" + i;
//            employeeService.insert(new Employee(i,name));
//        }
        System.out.println("---get all employee--");
        List<Employee> employeeList = employeeService.getAll();
        employeeList.forEach(System.out::println);
        System.out.println("---get employee by id---");
        Employee employeeByID = employeeService.getById(2L);
        System.out.println("----Update employee-----");
        Employee newEmployee = new Employee("new Employee");
        employeeService.update(5L,newEmployee);
//        System.out.println("-Delete employee with id");
//        employeeService.deleteEmployee(10L);
    }
}
