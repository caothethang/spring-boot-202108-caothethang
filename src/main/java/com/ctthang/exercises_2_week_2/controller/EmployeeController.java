package com.ctthang.exercises_2_week_2.controller;

import com.ctthang.exercises_2_week_2.entity.Employee;
import com.ctthang.exercises_2_week_2.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeController {
    
    @Autowired
    private EmployeeService employeeService;
    
    @PostMapping("/employees")
    private Employee create(@RequestBody Employee e){
         return employeeService.insert(e);
    }
    
}
