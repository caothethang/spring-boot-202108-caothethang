package com.ctthang.exercises_1_week_3.mapper;

import com.ctthang.exercises_1_week_3.dto.CryptoDTO;
import com.ctthang.exercises_1_week_3.entity.Crypto;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class chuyển đổi giữ entity và DTO
 */
@Component
public class CryptoMapper {
    public CryptoDTO toDTO(Crypto crypto) {
        CryptoDTO cryptoDTO = new CryptoDTO();
        cryptoDTO.setId(crypto.getId());
        cryptoDTO.setAddress(crypto.getAddress());
        cryptoDTO.setSymbol(crypto.getSymbol());
        cryptoDTO.setName(crypto.getName());
        cryptoDTO.setCreated_at(crypto.getCreated_at());
        cryptoDTO.setUpdate_at(crypto.getUpdated_at());
        cryptoDTO.setCoin_gecko_id(crypto.getCoin_gecko_id());
        return cryptoDTO;
    }
    
    public Crypto toCrypto(CryptoDTO cryptoDTO) {
        Crypto crypto = new Crypto();
        crypto.setName(cryptoDTO.getName());
        crypto.setAddress(cryptoDTO.getAddress());
        crypto.setCoin_gecko_id(cryptoDTO.getCoin_gecko_id());
        crypto.setSymbol(cryptoDTO.getSymbol());
        crypto.setCreated_at(cryptoDTO.getCreated_at());
        crypto.setUpdated_at(cryptoDTO.getUpdate_at());
        return crypto;
    }
    
    public List<CryptoDTO> toListDTO(List<Crypto> cryptos) {
        List<CryptoDTO> cryptoDTOList = new ArrayList<>();
        cryptos.forEach(crypto -> cryptoDTOList.add(toDTO(crypto)));
        return cryptoDTOList;
    }
    
    
}
