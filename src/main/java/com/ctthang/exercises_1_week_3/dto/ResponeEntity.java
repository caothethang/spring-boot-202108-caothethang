package com.ctthang.exercises_1_week_3.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ResponeEntity {
    private int status;
    private String message;
    private Object data;
}
