package com.ctthang.exercises_1_week_3.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CryptoExchangeDTO {
    private String symbol;
    private String name;
    private BigDecimal current_price;
}
