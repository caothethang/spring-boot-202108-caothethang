package com.ctthang.exercises_1_week_3.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class CryptoDTO {
    private Long id;
    private String symbol;
    private String address;
    private String name;
    private String coin_gecko_id;
    private Date created_at;
    private Date update_at;
}
