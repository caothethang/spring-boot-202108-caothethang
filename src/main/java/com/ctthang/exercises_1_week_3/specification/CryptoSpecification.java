package com.ctthang.exercises_1_week_3.specification;

import com.ctthang.exercises_1_week_3.entity.Crypto;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

public class CryptoSpecification {
    public static Specification<Crypto> filterForCrypto(String symbol,String address,String name){
        return (root, query, criteriaBuilder) -> {
            Collection<Predicate> predicates = new ArrayList<>();
            if(!Objects.isNull(symbol)){
                predicates.add(criteriaBuilder.like(root.get("symbol"),"%"+symbol+"%"));
            }
            if (!Objects.isNull(address)){
                predicates.add(criteriaBuilder.like(root.get("address"),"%"+address+"%"));
            }
            if(!Objects.isNull(name)){
                predicates.add(criteriaBuilder.like(root.get("name"),"%"+name+"%"));
            }
            return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
        };
    }
}
