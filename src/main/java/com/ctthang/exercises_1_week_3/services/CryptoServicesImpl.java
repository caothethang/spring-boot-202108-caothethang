package com.ctthang.exercises_1_week_3.services;

import com.ctthang.exercises_1_week_3.dto.CryptoDTO;
import com.ctthang.exercises_1_week_3.dto.CryptoExchangeDTO;
import com.ctthang.exercises_1_week_3.dto.ResponeEntity;
import com.ctthang.exercises_1_week_3.entity.Crypto;
import com.ctthang.exercises_1_week_3.mapper.CryptoMapper;
import com.ctthang.exercises_1_week_3.repository.CryptoRateHistoryRepository;
import com.ctthang.exercises_1_week_3.repository.CryptoRepository;
import com.ctthang.exercises_1_week_3.specification.CryptoSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CryptoServicesImpl implements CryptoServices {
    
    @Autowired
    private CryptoRepository cryptoRepository;
    
    @Autowired
    private CryptoMapper cryptoMapper;
    
    private WebClient webClient;
    
    @Autowired
    private CryptoRateHistoryRepository cryptoRateHistoryRepository;
    
    private final String COIN_GECKO_URL = "https://api.coingecko.com";
    private final String COIN_GECKO_BY_IDS = "/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=100&page=1&sparkline=false&ids={listID}";
    
    public CryptoServicesImpl() {
        this.webClient = WebClient.create(COIN_GECKO_URL);
    }
    
    /**
     * Lấy danh sách crypto với phân trang , sắp xếp theo symbol tăng dần
     *
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @Override
    public List<CryptoDTO> getAll(int pageIndex, int pageSize) {
        Pageable paging = PageRequest.of(pageIndex, pageSize, Sort.by("symbol").ascending());
        Page<Crypto> cryptoPage = cryptoRepository.findAll(paging);
        List<Crypto> cryptoList = cryptoPage.getContent();
        return cryptoMapper.toListDTO(cryptoList);
    }
    
    /**
     * Lấy ra thông tin một crypto
     *
     * @param id
     * @return cryptoDTO
     */
    @Override
    public CryptoDTO getCryptoById(Long id) {
        Crypto crypto = cryptoRepository.findById(id).get();
        return cryptoMapper.toDTO(crypto);
    }
    
    /**
     * Tạo mới một crypto
     *
     * @param cryptoDTO
     * @return cryptoDTO : thông tin của crypto mới tạo
     */
    @Override
    public CryptoDTO createNewCrypto(CryptoDTO cryptoDTO) {
        return saveAndReturnDTO(cryptoMapper.toCrypto(cryptoDTO));
    }
    
    /**
     * Cập nhật thông tin một crypto theo id
     *
     * @param id
     * @param cryptoDTO
     * @return thông tin của crypto vừa cập nhật
     */
    @Override
    public CryptoDTO updateCryptoById(Long id, CryptoDTO cryptoDTO) {
        Crypto crypto = cryptoMapper.toCrypto(cryptoDTO);
        crypto.setId(id);
        return saveAndReturnDTO(crypto);
    }
    
    /**
     * Hàm thực hiện lưu và trả ra thông tin crypto
     *
     * @param crypto
     * @return thông tin của crypto vừa thực hiện lưu
     */
    private CryptoDTO saveAndReturnDTO(Crypto crypto) {
        Crypto savedCrypto = cryptoRepository.save(crypto);
        return cryptoMapper.toDTO(savedCrypto);
    }
    
    /**
     * Xoá một crypto khỏi cơ sở dữ liệu
     *
     * @param id
     */
    @Override
    public void deleteCryptoById(Long id) {
        cryptoRepository.deleteById(id);
    }
    
    @Override
    public List<CryptoDTO> findCryptoBySymbolAndNameAndAddress(String symbol, String name, String address, int pageIndex, int pageSize) {
        Pageable pageable = PageRequest.of(pageIndex, pageSize, Sort.by("symbol").ascending());
        Page<Crypto> page = cryptoRepository.findAll(CryptoSpecification.filterForCrypto(symbol, address, name), pageable);
        List<Crypto> cryptos = page.getContent();
        return cryptoMapper.toListDTO(cryptos);
    }
    
    /**
     * lấy tỷ giá của một symbol
     *
     * @param symbol
     * @return
     */
    @Override
    public List<CryptoExchangeDTO> getCurrentPriceOfSymbol(String symbol) {
        // lấy ra các crypto có symbol cần tìm
        List<Crypto> cryptoFoundBySymbol = cryptoRepository.findBySymbol(symbol);
        // Tạo chuỗi chưa các coin_gecko_id của các crypto tìm được
        String ids = cryptoFoundBySymbol.stream()
                .map(crypto -> String.valueOf(crypto.getCoin_gecko_id()))
                .collect(Collectors.joining(","));
        List<CryptoExchangeDTO> respone = getCurrentPriceOfSymbols(ids);
        return respone;
    }
    
    @Override
    public BigDecimal exchangeSymbolTo(String fromSymbol, BigDecimal fromAmount, String toSymbol) {
        String fromSymbolGeckoId = cryptoRepository.findBySymbol(fromSymbol).get(0).getCoin_gecko_id();
        String toSymbolGeckoId = cryptoRepository.findBySymbol(toSymbol).get(0).getCoin_gecko_id();
        String ids = fromSymbolGeckoId+","+toSymbolGeckoId;
        List<CryptoExchangeDTO> respone =getCurrentPriceOfSymbols(ids);
        BigDecimal fromSymbolPrice = null,toSymbolPrice = null;
        for (CryptoExchangeDTO cryptoExchangeDTO : respone){
            if(cryptoExchangeDTO.getSymbol().equalsIgnoreCase(fromSymbol)){
                fromSymbolPrice = cryptoExchangeDTO.getCurrent_price();
            }
            if(cryptoExchangeDTO.getSymbol().equalsIgnoreCase(toSymbol)){
                toSymbolPrice = cryptoExchangeDTO.getCurrent_price();
            }
        }
        BigDecimal toAmount = fromAmount.multiply(fromSymbolPrice.divide(toSymbolPrice,5, RoundingMode.CEILING));
        return toAmount;
    }
    
    @Override
    public List<String> getAllCoinGeckoId() {
        List<Crypto> cryptos = cryptoRepository.findAll();
        List<String> symbols = cryptos.stream().map(crypto -> crypto.getCoin_gecko_id()).collect(Collectors.toList());
        return symbols;
    }
    
    @Override
    public List<CryptoExchangeDTO> getCurrentPriceOfSymbols(String ids){
        List<CryptoExchangeDTO> respone = webClient.get()
                .uri(COIN_GECKO_BY_IDS, ids)
                .retrieve()
                .bodyToFlux(CryptoExchangeDTO.class)
                .collectList()
                .block();
        return respone;
    }
    
    @Override
    public ResponeEntity getPriceOfSymbolAtTime(String symbol, Long timeAt) {
        Long crypto_id = cryptoRepository.findBySymbol(symbol).get(0).getId();
        BigDecimal priceAtTime = cryptoRateHistoryRepository.getPriceAtTimeNearest(crypto_id,timeAt);
        return new ResponeEntity(200,"Price at: ",priceAtTime);
    }
}
