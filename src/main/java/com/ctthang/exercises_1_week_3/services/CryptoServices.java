package com.ctthang.exercises_1_week_3.services;

import com.ctthang.exercises_1_week_3.dto.CryptoDTO;
import com.ctthang.exercises_1_week_3.dto.CryptoExchangeDTO;
import com.ctthang.exercises_1_week_3.dto.ResponeEntity;
import org.springframework.data.jpa.repository.Query;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

public interface CryptoServices {
    List<CryptoDTO> getAll(int pageIndex,int pageSize);
    
    CryptoDTO getCryptoById(Long id);
    
    CryptoDTO createNewCrypto(CryptoDTO cryptoDTO);
    
    CryptoDTO updateCryptoById(Long id, CryptoDTO cryptoDTO);
    
    void deleteCryptoById(Long id);
    
    List<CryptoDTO> findCryptoBySymbolAndNameAndAddress(String symbol,String name,String address,int pageIndex,int pageSize);
    
    List<CryptoExchangeDTO> getCurrentPriceOfSymbol(String symbol);
    
    BigDecimal exchangeSymbolTo(String fromSymbol,BigDecimal fromAmount,String toSymbol);
    
    List<String> getAllCoinGeckoId();
    
    List<CryptoExchangeDTO> getCurrentPriceOfSymbols(String ids);
    
    ResponeEntity getPriceOfSymbolAtTime(String symbol, Long timeAt);
}
