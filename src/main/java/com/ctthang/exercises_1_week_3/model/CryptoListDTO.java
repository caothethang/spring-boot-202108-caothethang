package com.ctthang.exercises_1_week_3.model;

import com.ctthang.exercises_1_week_3.dto.CryptoDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Danh sách các crypto
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CryptoListDTO {
    List<CryptoDTO> cryptos;
}
