package com.ctthang.exercises_1_week_3.entity;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Date;

@Entity
@Table(name = "crypto_rate_history")
@Data
@EntityListeners(AuditingEntityListener.class)
@NoArgsConstructor
@ApiModel
public class CryptoRateHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;
    
    @Column(name = "current_price")
    private BigDecimal current_price;
    
    @Column(name = "create_at")
    @CreatedDate
    private Timestamp create_at;
    
    @ManyToOne
    @JoinColumn(name = "crypto_id")
    private Crypto crypto;
    
    public CryptoRateHistory(BigDecimal current_price, Timestamp timestamp, Crypto c) {
        this.current_price = current_price;
        this.create_at=timestamp;
        this.crypto = c;
    }
}
