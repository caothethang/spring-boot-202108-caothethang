package com.ctthang.exercises_1_week_3.entity;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.sql.Timestamp;
import java.util.Date;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@ApiModel
public class Crypto {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;
    
    @Column(name = "symbol", nullable = false)
    private String symbol;
    
    @Column(name = "address")
    @NotBlank(message = "address is mandatory")
    private String address;
    
    @Column(name = "created_at")
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date created_at;
    
    @Column(name = "updated_at")
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updated_at;
    
    @Column(name = "is_deleted")
    private Boolean is_deleted;
    
    @Column(name = "whitelist_collateral", nullable = false)
    private boolean whitelist_collateral;
    
    @Column(name = "whitelist_supply", nullable = false)
    private boolean whitelist_supply;
    
    @Column(name = "name", nullable = false)
    private String name;
    
    @Column(name = "coin_gecko_id", nullable = false)
    private String coin_gecko_id;
    
}
