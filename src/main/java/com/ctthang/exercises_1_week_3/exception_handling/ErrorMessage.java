package com.ctthang.exercises_1_week_3.exception_handling;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class ErrorMessage {
    public int statusCode;
    public String message;
    public List<String> details;
}
