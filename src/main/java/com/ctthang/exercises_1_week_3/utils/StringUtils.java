package com.ctthang.exercises_1_week_3.utils;

import java.util.List;
import java.util.stream.Collectors;

public class StringUtils {
    
    /**
     * Hàm chuyển list string về string nối với nhau bởi ","
     * @param strings
     * @return
     */
    public static String convertListToString(List<String> strings){
        return strings.stream().collect(Collectors.joining(","));
    }
}
