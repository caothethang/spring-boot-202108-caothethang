package com.ctthang.exercises_1_week_3.controller;

import com.ctthang.exercises_1_week_3.dto.CryptoDTO;
import com.ctthang.exercises_1_week_3.dto.CryptoExchangeDTO;
import com.ctthang.exercises_1_week_3.dto.ResponeEntity;
import com.ctthang.exercises_1_week_3.model.CryptoListDTO;
import com.ctthang.exercises_1_week_3.services.CryptoServices;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(CryptoController.URL)
@Api
public class CryptoController {
    
    public static final String URL = "/api/cryptos";
    
    private final CryptoServices cryptoServices;
    
    public CryptoController(CryptoServices cryptoServices) {
        this.cryptoServices = cryptoServices;
    }
    
    /**
     * api lấy danh sách crypto theo phân trang
     *
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public CryptoListDTO getListCrypto(@RequestParam(value = "pageIndex", defaultValue = "0", required = false) int pageIndex,
                                       @RequestParam(value = "pageSize", defaultValue = "5", required = false) int pageSize) {
        return new CryptoListDTO(cryptoServices.getAll(pageIndex, pageSize));
    }
    
    /**
     * api lấy thông tin một crypto theo id
     *
     * @param id
     * @return
     */
    @GetMapping({"/{id}"})
    @ResponseStatus(HttpStatus.OK)
    public CryptoDTO getCryptoById(@PathVariable long id) {
        return cryptoServices.getCryptoById(id);
    }
    
    /**
     * api tìm kiếm theo symbol
     *
     * @param symbol
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @GetMapping({"/search"})
    @ResponseStatus(HttpStatus.OK)
    public List<CryptoDTO> getCryptoFilter(@RequestParam(value = "symbol", required = false) String symbol,
                                             @RequestParam(value = "address", required = false) String address,
                                             @RequestParam(value = "name", required = false) String name,
                                             @RequestParam(defaultValue = "0") int pageIndex,
                                             @RequestParam(defaultValue = "5") int pageSize) {
        return cryptoServices.findCryptoBySymbolAndNameAndAddress(symbol, name, address , pageIndex, pageSize);
    }
    
    /**
     * Api lấy tỷ giá theo usd của 1 crypto
     * @param symbol
     * @return
     */
    @GetMapping({"/{symbol}/current-price"})
    @ApiOperation(value = "lấy tỷ giá theo USD của 1 crypto")
    @ResponseStatus(HttpStatus.OK)
    public List<CryptoExchangeDTO> getCurrentPriceOfSymbol(@PathVariable String symbol) {
        return cryptoServices.getCurrentPriceOfSymbol(symbol);
    }
    
    /**
     * Api lấy tỷ giá của 1 crypto tại 1 thời điểm
     * @param symbol
     * @param time
     * @return
     */
    @GetMapping({"/price"})
    @ApiOperation(value = "lấy giá của crypto tại thời điểm")
    @ResponseStatus(HttpStatus.OK)
    public ResponeEntity getPriceOfSymbolAtTime(@RequestParam (value = "symbol")String symbol,
                                                @RequestParam(value = "time_at")Long time){
        return cryptoServices.getPriceOfSymbolAtTime(symbol, time);
    }
    
    /**
     * api tạo mới một crypto
     *
     * @param cryptoDTO
     * @return
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CryptoDTO createNewCrypto(@RequestBody @Valid CryptoDTO cryptoDTO) {
        return cryptoServices.createNewCrypto(cryptoDTO);
    }
    
    /**
     * api cập nhật thông tin một crypto
     *
     * @param id
     * @param cryptoDTO
     * @return
     */
    @PutMapping({"/{id}"})
    @ResponseStatus(HttpStatus.OK)
    public CryptoDTO updateCrypto(@PathVariable long id, @RequestBody CryptoDTO cryptoDTO) {
        return cryptoServices.updateCryptoById(id, cryptoDTO);
    }
    
    /**
     * api xoá một crypto theo id
     *
     * @param id
     */
    @DeleteMapping({"/{id}"})
    @ResponseStatus(HttpStatus.OK)
    public void deleteCrypto(@PathVariable long id) {
        cryptoServices.deleteCryptoById(id);
    }
    
}
