package com.ctthang.exercises_1_week_3.controller;

import com.ctthang.exercises_1_week_3.services.CryptoServices;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
@Api
public class ExchangeCryptoController {
    
    @Autowired
    private CryptoServices cryptoServices;
    
    @GetMapping({"/exchange"})
    @ResponseStatus(HttpStatus.OK)
    public BigDecimal exchangeCrypto(@RequestParam(value = "fromSymbol") String fromSymbol,
                                     @RequestParam(value = "fromAmount") BigDecimal fromAmount,
                                     @RequestParam(value = "toSymbol") String toSymbol){
        return cryptoServices.exchangeSymbolTo(fromSymbol, fromAmount, toSymbol);
    }
}
