package com.ctthang.exercises_1_week_3.repository;

import com.ctthang.exercises_1_week_3.entity.CryptoRateHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

@Repository
public interface CryptoRateHistoryRepository extends JpaRepository<CryptoRateHistory,Long> {
    
    @Modifying
    @Query(value = "delete from crypto_rate_history c where TIMESTAMPDIFF(MINUTE ,c.create_at,TIMESTAMP(NOW())) > ?1",nativeQuery = true)
    @Transactional(rollbackFor = Exception.class)
    void deleteRateHistoryAfterTime(int timeExpired);
    
    @Query(value = "select current_price from crypto_rate_history " +
            "where crypto_id = ?1 AND UNIX_TIMESTAMP(create_at) <= ?2 " +
            "ORDER BY create_at desc limit 1",nativeQuery = true)
    BigDecimal getPriceAtTimeNearest(Long cryptoId,Long timeAt);
}
