package com.ctthang.exercises_1_week_3.repository;

import com.ctthang.exercises_1_week_3.entity.Crypto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CryptoRepository extends JpaRepository<Crypto, Long>, JpaSpecificationExecutor<Crypto> {
    
    List<Crypto> findBySymbol(String symbol);
}
