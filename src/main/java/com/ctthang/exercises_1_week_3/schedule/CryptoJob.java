package com.ctthang.exercises_1_week_3.schedule;

import com.ctthang.exercises_1_week_3.dto.CryptoExchangeDTO;
import com.ctthang.exercises_1_week_3.entity.Crypto;
import com.ctthang.exercises_1_week_3.entity.CryptoRateHistory;
import com.ctthang.exercises_1_week_3.mapper.CryptoMapper;
import com.ctthang.exercises_1_week_3.repository.CryptoRateHistoryRepository;
import com.ctthang.exercises_1_week_3.repository.CryptoRepository;
import com.ctthang.exercises_1_week_3.services.CryptoServices;
import com.ctthang.exercises_1_week_3.utils.StringUtils;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;
import java.util.List;

@Component
public class CryptoJob {
    
    private final CryptoRepository cryptoRepository;
    private final CryptoServices cryptoServices;
    private final List<String> COINGECKO_ID_LIST;
    private final String COINGECKO_LIST_PARAM;
    private final CryptoMapper cryptoMapper;
    private List<Crypto> cryptos;
    private final CryptoRateHistoryRepository cryptoRateHistoryRepository;
    
    public CryptoJob(CryptoRepository cryptoRepository,CryptoMapper cryptoMapper,
                     CryptoServices cryptoServices,CryptoRateHistoryRepository cryptoRateHistoryRepository){
        this.cryptoRepository = cryptoRepository;
        this.cryptoMapper = cryptoMapper;
        this.cryptoServices = cryptoServices;
        this.COINGECKO_ID_LIST = cryptoServices.getAllCoinGeckoId();
        this.COINGECKO_LIST_PARAM = StringUtils.convertListToString(COINGECKO_ID_LIST);
        this.cryptoRateHistoryRepository = cryptoRateHistoryRepository;
    }
    
    /**
     *  Lập lịch chạy mỗi 1 phút
     *  Xoá những bản ghi quá hạn 60p
     *  cập nhật tỷ giá của các crypto
     */
//    @Scheduled(cron = "0 * * * * *")
//    public void getPriceForAllCryptoPerMinute(){
//        cryptoRateHistoryRepository.deleteRateHistoryAfterTime(60);
//        List<CryptoExchangeDTO> cryptoCurrentPriceList = cryptoServices.getCurrentPriceOfSymbols(COINGECKO_LIST_PARAM);
//        cryptos = cryptoRepository.findAll();
//        for(Crypto c : cryptos){
//            for(CryptoExchangeDTO cryptoExchangeDTO : cryptoCurrentPriceList){
//                if(c.getSymbol().equalsIgnoreCase(cryptoExchangeDTO.getSymbol())){
//                    CryptoRateHistory cryptoRateHistory = new CryptoRateHistory(cryptoExchangeDTO.getCurrent_price(),new Timestamp(System.currentTimeMillis()),c);
//                    cryptoRateHistoryRepository.saveAndFlush(cryptoRateHistory);
//                    cryptoCurrentPriceList.remove(cryptoExchangeDTO);
//                    break;
//                }
//            }
//        }
//    }
}
