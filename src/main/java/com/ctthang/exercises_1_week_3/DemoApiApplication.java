package com.ctthang.exercises_1_week_3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableScheduling;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableJpaAuditing
@EnableSwagger2
@EnableScheduling
@EnableEurekaClient
@Profile(value = "crypto")
public class DemoApiApplication{
    public static void main(String[] args) {
        SpringApplication.run(DemoApiApplication.class);
    }
}
