package com.ctthang.exercises_3_week_2.entity;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.*;

@Entity
@Data
// set index cho bảng person
//@Table(name = "Person", indexes = {
//        @Index(name = "idx_person_fullname", columnList = "fullname"),
//        @Index(name = "idx_person_id", columnList = "id")
//})
@NoArgsConstructor
public class Person {
    
    public Person(String fullName) {
        this.fullName = fullName;
    }
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    
    @Column(name = "fullname")
    @NonNull
    private String fullName;
    
}
