package com.ctthang.exercises_3_week_2;


import com.ctthang.exercises_3_week_2.entity.Person;
import com.ctthang.exercises_3_week_2.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.awt.print.Book;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
public class DemoIndexApplication implements CommandLineRunner {
    
    @Autowired
    private PersonRepository personRepository;
    
    public static void main(String[] args) {
        SpringApplication.run(DemoIndexApplication.class);
    }
    
    @Override
    public void run(String... args) throws Exception {
        long startTimeInsertPerson = System.nanoTime();
        initDataForPerson();
        long endTimeInsertPerson = System.nanoTime();
        System.out.println("Elapsed Time insert person in nano seconds: "+ (endTimeInsertPerson-startTimeInsertPerson));
        long startTimeFindPerson = System.nanoTime();
        findPerson();
        long endTimeFindPerson = System.nanoTime();
        System.out.println("Elapsed Time find person in nano seconds: "+ (endTimeFindPerson-startTimeFindPerson));
    }
    
    private void findPerson() {
        Person person = personRepository.findByName("Person 9999");
        System.out.println(person.getFullName());
    }
    
    
    public void initDataForPerson(){
        List<Person> listPerson = new ArrayList<>();
        for(int i=1;i<=2000000;i++){
            Person person = new Person("Person "+i);
            listPerson.add(person);
        }
        personRepository.saveAll(listPerson);
        
    }
}
