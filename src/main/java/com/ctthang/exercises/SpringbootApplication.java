package com.ctthang.exercises;

import com.ctthang.exercises.entity.Customer;
import com.ctthang.exercises.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootApplication implements CommandLineRunner {
    
    @Autowired
    private CustomerRepository customerRepository;
    
    public static void main(String[] args) {
        SpringApplication.run(SpringbootApplication.class, args);
    }
    
    @Override
    public void run(String... args) throws Exception {
        Customer customer = new Customer(1L,"Thang");
        customerRepository.insert(customer);
//        customerRepository.update("Sang",customer.getId());
        /*
            Tạo id trùng để test transaction
         */
//        Customer customer1 = new Customer(1L,"Sang");
//        customerRepository.insert(customer1);
    }
}
