package com.ctthang.exercises.repository;

import com.ctthang.exercises.entity.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

@Repository
@Transactional(rollbackFor = {Exception.class})
public class CustomerRepository {
    
    @Autowired
    private EntityManager entityManager;
    
    // sử dụng native SQL
    public void insert(Customer customer) throws Exception {
        entityManager.createNativeQuery("INSERT INTO CUSTOMER(id,name) VALUES (?,?)")
                .setParameter(1,customer.getId())
                .setParameter(2,customer.getName())
                .executeUpdate();
        System.out.println("----Insert dữ liệu thành công------");
//        sử dụng hàm exception để test transaction
//        demoException();
    }
    /*
        @Query sử dụng bên trong interface
     */
//    @Modifying
//    @Query("UPDATE Customer set name=:name where id=:id")
//    public void update(@Param("name") String name,@Param("id") long id);
    
    public void demoException() throws Exception {
        // do something
        throw new Exception("demo throw exception");
    }
}
