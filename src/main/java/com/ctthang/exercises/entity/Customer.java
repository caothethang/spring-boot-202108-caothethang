package com.ctthang.exercises.entity;

import javax.persistence.*;

@Entity
public class Customer {
    @Id
    @Column(name = "id", nullable = false)
    private Long id;
    
    @Column(name = "name",nullable = false)
    private String name;
    
    public Customer(Long id, String name) {
        this.id = id;
        this.name = name;
    }
    
    public Customer() {
    
    }
    
    public Long getId() {
        return id;
    }
    
    public void setId(Long id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
}
