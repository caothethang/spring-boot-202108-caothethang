package com.ctthang.exercises_4_week_2.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;

@Document(collection = "Employee")
@Data
@NoArgsConstructor
@ToString
@AllArgsConstructor
public class Employee {
    
    @Id
    private Long id;
    private String name;
    
}
