package com.ctthang.exercises_4_week_2;

import com.ctthang.exercises_4_week_2.entity.Employee;
import com.ctthang.exercises_4_week_2.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
public class DemoMongoDbApplication implements CommandLineRunner {
    
    @Autowired
    private EmployeeRepository employeeRepository;
    
    public static void main(String[] args) {
        SpringApplication.run(DemoMongoDbApplication.class);
    }
    
    @Override
    public void run(String... args) throws Exception {
//        insertEmployee();
        showAllEmployee();
        System.out.println("------Tìm kiếm theo ID");
        getEmployeeById(5);
        System.out.println("------Cập nhật employee có id = 5 :    ------");
        updateEmployee(5);
        System.out.println("------Xoá employee có id =4: ---------");
        deleteEmployee(4);
        System.out.println("------ Sau khi xoá và cập nhật: --------------");
        showAllEmployee();
    }
    
    /*
        Hiển thị toàn bộ documents
     */
    public void showAllEmployee() {
        List<Employee> listEmployee = employeeRepository.findAll();
        listEmployee.forEach(System.out::println);
    }
    /*
        Tìm kiếm theo id
     */
    private void getEmployeeById(long id){
        try {
            Employee employee = employeeRepository.findById(id).get();
            System.out.println(employee);
        }catch (Exception e){
            System.out.println("Không tồn tại employee với id này");
        }
    }
    
    /*
        Thêm một document vào collection
     */
    public void insertEmployee() {
        for (long i = 1; i <= 10; i++){{
            Employee e = new Employee(i,"Employee"+ i);
            employeeRepository.save(e);
        }}
    }
    
    /*
        cập nhật Employee
     */
    private void updateEmployee(long id){
        try {
            Employee employee = employeeRepository.findById(id).get();
            employee.setName("Cao The Thang");
            employeeRepository.save(employee);
        } catch (Exception e) {
            System.out.println("Không tồn tại Id");
            e.printStackTrace();
        }
    }
    /*
        Xoá employee
     */
    private void deleteEmployee(long id){
        try {
            employeeRepository.deleteById(id);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
