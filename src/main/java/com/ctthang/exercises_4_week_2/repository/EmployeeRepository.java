package com.ctthang.exercises_4_week_2.repository;

import com.ctthang.exercises_4_week_2.entity.Employee;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeRepository extends MongoRepository<Employee,Long> {
    
    @Query("{ 'name': ?0}")
    List<Employee> findByName(String name);
}
