package com.ctthang.exercises_1_week_3.services;

import com.ctthang.exercises_1_week_3.dto.CryptoDTO;
import com.ctthang.exercises_1_week_3.entity.Crypto;
import com.ctthang.exercises_1_week_3.repository.CryptoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;

class CryptoServicesImplTest {
    
    private CryptoRepository cryptoRepository = Mockito.mock(CryptoRepository.class);
    private CryptoServices cryptoServices = new CryptoServicesImpl();
    
    @BeforeEach
    void init(){
        Crypto crypto = new Crypto();
        crypto.setName("XPR");
        doReturn(Optional.of(crypto)).when(cryptoRepository).findById(1L);
        List<Crypto> cryptoList = Arrays.asList(crypto);
        doReturn(cryptoList).when(cryptoRepository.findAll());
    }
    
    @Test
    @DisplayName("find all can return list")
    void getAll() {
        Iterable<CryptoDTO> cryptoList = cryptoServices.getAll(3,5);
        assertThat(cryptoList).hasSize(1);
    }
    
    @Test
    void getCryptoById() {
    }
    
    @Test
    void createNewCrypto() {
    }
    
    @Test
    void updateCryptoById() {
    }
    
    @Test
    void deleteCryptoById() {
    }
}